/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 14.355300859598854, "KoPercent": 85.64469914040114};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.005157593123209169, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.010610079575596816, 500, 1500, "getChildHealthRecords"], "isController": false}, {"data": [0.02361111111111111, 500, 1500, "getClassAttendanceSummaries"], "isController": false}, {"data": [0.0, 500, 1500, "getChildStatementOfAccount"], "isController": false}, {"data": [0.0, 500, 1500, "childByID"], "isController": false}, {"data": [0.0, 500, 1500, "findClassActivities"], "isController": false}, {"data": [0.0, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.0, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [0.015068493150684932, 500, 1500, "bankAccountsByFkChild"], "isController": false}, {"data": [0.0, 500, 1500, "getChildrenData"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3490, 2989, 85.64469914040114, 45509.02722063041, 7, 600848, 10149.5, 144891.5, 175636.45, 176816.18, 4.915936084379437, 2.6785039866269633, 17.701953283464988], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getChildHealthRecords", 377, 254, 67.37400530503979, 26384.562334217488, 60, 175909, 9003.0, 92863.4, 100853.7, 114739.83999999998, 1.1679637156736393, 0.60189849287759, 2.8183968178023076], "isController": false}, {"data": ["getClassAttendanceSummaries", 360, 234, 65.0, 12876.877777777776, 7, 117696, 9003.0, 18968.9, 46708.49999999999, 102239.14999999994, 1.1152969168236344, 0.6179877642634084, 1.1871812884157826], "isController": false}, {"data": ["getChildStatementOfAccount", 407, 407, 100.0, 57636.68550368553, 200, 177863, 10494.0, 175589.0, 175923.8, 177708.44, 1.2609051254089423, 0.6710872005350327, 2.055127592097192], "isController": false}, {"data": ["childByID", 401, 395, 98.50374064837905, 57206.927680798006, 1939, 177920, 10562.0, 175500.2, 175891.9, 176939.7, 1.2422745085890428, 0.6941324989544447, 15.814736810514413], "isController": false}, {"data": ["findClassActivities", 397, 391, 98.48866498740554, 61844.607052896725, 6707, 177657, 10655.0, 175484.4, 175861.4, 176823.38, 1.2298636926889717, 0.6677805771762702, 4.030686086586122], "isController": false}, {"data": ["getAllClassInfo", 413, 398, 96.3680387409201, 62507.6440677966, 9001, 177858, 10758.0, 175517.2, 175872.5, 176934.82, 1.2794418783322026, 0.7266110018448126, 3.284817088022813], "isController": false}, {"data": ["getChildrenToAssignToClass", 388, 388, 100.0, 60843.47938144328, 6685, 177714, 10515.5, 175601.3, 175935.65, 177379.07, 1.2019863754224764, 0.6397290767629392, 4.778600131196193], "isController": false}, {"data": ["bankAccountsByFkChild", 365, 249, 68.21917808219177, 29008.70410958905, 39, 600848, 9003.0, 95604.40000000014, 122292.09999999977, 176067.13999999998, 0.5141322996222184, 0.28257055629466965, 0.6592340912148171], "isController": false}, {"data": ["getChildrenData", 382, 273, 71.46596858638743, 34770.489528795806, 1657, 176596, 10343.0, 100950.6, 112828.89999999994, 158238.93, 1.183409955513699, 0.6502873525384454, 3.513248305431294], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 2989, 100.0, 85.64469914040114], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3490, 2989, "502/Bad Gateway", 2989, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["getChildHealthRecords", 377, 254, "502/Bad Gateway", 254, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getClassAttendanceSummaries", 360, 234, "502/Bad Gateway", 234, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildStatementOfAccount", 407, 407, "502/Bad Gateway", 407, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["childByID", 401, 395, "502/Bad Gateway", 395, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findClassActivities", 397, 391, "502/Bad Gateway", 391, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getAllClassInfo", 413, 398, "502/Bad Gateway", 398, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildrenToAssignToClass", 388, 388, "502/Bad Gateway", 388, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["bankAccountsByFkChild", 365, 249, "502/Bad Gateway", 249, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildrenData", 382, 273, "502/Bad Gateway", 273, null, null, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
